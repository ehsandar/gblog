project file structure:
https://github.com/golang-standards/project-layout

command line arg parser: cobra
https://github.com/spf13/cobra

inject custom tag to structs:
https://github.com/favadi/protoc-go-inject-tag

##grpcurl commands:

* normal get post
```shell script
grpcurl -plaintext localhost:8080 posts.PostStorage/GetPosts
```

* add new post
```shell script
grpcurl -d '{"post": {"title": "Test 2", "body": "this is body 2"}}' -plaintext localhost:8080 posts.PostStorage/AddPost
```

###Auth

* create new user
```shell script
grpcurl -d '{"user": {"username":"ehsan"}, "raw_password":"testtest"}' -plaintext localhost:8080 auth.Auth/AddUser
```

* get auth token (JWT)
```shell script
grpcurl -d '{"username":"ehsan", "password":"testtest"}' -plaintext localhost:8080 auth.Auth/GetToken
```

* for getting posts after auth interceptor added
```shell script
grpcurl -H auth:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVoc2FuIn0.4EyWKAhSJpNsjUo0kSopFr0-gl6SKp5ZcMspkk5j7lE" -plaintext localhost:8080 posts.PostStorage/GetPosts
```



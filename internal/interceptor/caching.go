package interceptor

import (
	"context"
	"crypto/sha1"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/ehsandar/gblog/pkg/gblog/post"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
)

func NewCachingInterceptor(rdb redis.Conn) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (resp interface{}, err error) {
		if info.FullMethod != "/posts.PostStorage/GetPosts" {
			return handler(ctx, req)
		}

		reqBin, err := proto.Marshal(req.(proto.Message))
		if err != nil {
			return nil, err
		}

		hc := sha1.New()
		hc.Write(reqBin)
		sha1sumHex := fmt.Sprintf("%x", hc.Sum(nil))

		cachedBin, err := redis.Bytes(rdb.Do("GET", sha1sumHex))
		if cachedBin != nil {
			resp = &post.GetPostsResponse{}
			err = proto.Unmarshal(cachedBin, resp.(proto.Message))
			if err != nil {
				return nil, err
			}
			return resp, nil
		} else {
			resp, err = handler(ctx, req)
			if err != nil {
				return nil, err
			}
			respBin, err := proto.Marshal(resp.(proto.Message))
			if err != nil {
				return resp, err
			}
			_, err = rdb.Do("SETEX", sha1sumHex, 120, respBin)
			return resp, err
		}
	}
}

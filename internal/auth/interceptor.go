package auth

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func NewAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (resp interface{}, err error) {
		if info.FullMethod != "/posts.PostStorage/GetPosts" {
			return handler(ctx, req)
		}

		headers, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Error(codes.DataLoss, "can not access header info")
		}
		authJwt, ok := headers["auth"]
		if !ok {
			return nil, status.Error(codes.Unauthenticated, "auth header not provided")
		}

		_, err = jwt.Parse(authJwt[0], func(token *jwt.Token) (interface{}, error) {
			return []byte("SecretHitler"), nil
		})
		if err != nil {
			return nil, status.Errorf(codes.Unauthenticated, "invalid auth: %s", err)
		}

		return handler(ctx, req)
	}
}

package auth

import (
	"context"
	"crypto/sha1"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/prometheus/common/log"
	gblog "gitlab.com/ehsandar/gblog/pkg"
	"gitlab.com/ehsandar/gblog/pkg/gblog/auth"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func New(authDb *gorm.DB) auth.AuthServer {
	return &authService{
		db: authDb,
	}
}

type authService struct {
	db *gorm.DB
}

func (a *authService) AddUser(ctx context.Context, request *auth.AddUserRequest) (*auth.AddUserResponse, error) {
	a.db.Create(&gblog.User{
		Username: request.User.Username,
		Password: hashPassword(request.RawPassword, 12),
	})
	return &auth.AddUserResponse{}, nil
}

func (a *authService) GetToken(ctx context.Context, request *auth.GetTokenRequest) (*auth.GetTokenResponse, error) {
	user := &gblog.User{}
	a.db.First(user, "username=?", request.Username)
	if user.Username == "" {
		return nil, status.Error(codes.NotFound, "user not found")
	}

	if hashPassword(request.Password, 12) != user.Password {
		return nil, status.Error(codes.Unauthenticated, "username and password does not match")
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
	}).SignedString([]byte("SecretHitler"))
	if err != nil {
		log.Error(err)
		return nil, err
	}
	return &auth.GetTokenResponse{Token: token}, nil
}

func hashPassword(password string, times int) string {
	for i := 0; i < times; i++ {
		hash := sha1.New()
		hash.Write([]byte(password))
		password = string(hash.Sum(nil))
	}
	return password
}

package middleware

import (
	"context"
	"fmt"
	gblog "gitlab.com/ehsandar/gblog/pkg"
	"gitlab.com/ehsandar/gblog/pkg/gblog/post"
)

func LoggingMiddlewareFactory(inx int) gblog.PostServiceMiddleware {
	return func(next post.PostStorageServer) post.PostStorageServer {
		return &loggingMiddleware{next: next, idx: inx}
	}
}

type loggingMiddleware struct {
	next post.PostStorageServer
	idx  int
}

func (m *loggingMiddleware) GetPosts(ctx context.Context, request *post.GetPostsRequest) (*post.GetPostsResponse, error) {
	fmt.Printf("Before GetPosts: %d\n", m.idx)
	resp, err := m.next.GetPosts(ctx, request)
	fmt.Printf("After GetPosts: %d\n", m.idx)
	return resp, err
}

func (m *loggingMiddleware) AddPost(ctx context.Context, request *post.AddPostRequest) (*post.AddPostResponse, error) {
	resp, err := m.next.AddPost(ctx, request)
	return resp, err
}

package post

import (
	"context"
	"fmt"
	"gitlab.com/ehsandar/gblog/pkg/gblog/post"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func New(collection *mongo.Collection) post.PostStorageServer {
	return &service{
		collection: collection,
	}
}

type service struct {
	collection *mongo.Collection
}

func (s *service) GetPosts(ctx context.Context, request *post.GetPostsRequest) (*post.GetPostsResponse, error) {
	fmt.Println("Getting posts from db")
	postsCur, err := s.collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	results := post.GetPostsResponse{}
	if err = postsCur.All(ctx, &results.Posts); err != nil {
		return nil, err
	}
	return &results, nil
}

func (s *service) AddPost(ctx context.Context, request *post.AddPostRequest) (*post.AddPostResponse, error) {
	_, err := s.collection.InsertOne(ctx, request.Post)
	if err != nil {
		return nil, err
	}
	return &post.AddPostResponse{}, nil
}

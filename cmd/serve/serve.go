package serve

import (
	"context"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/ehsandar/gblog/internal/auth"
	"gitlab.com/ehsandar/gblog/internal/interceptor"
	"gitlab.com/ehsandar/gblog/internal/middleware"
	"gitlab.com/ehsandar/gblog/internal/post"
	gblog "gitlab.com/ehsandar/gblog/pkg"
	authPb "gitlab.com/ehsandar/gblog/pkg/gblog/auth"
	postPb "gitlab.com/ehsandar/gblog/pkg/gblog/post"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

var Cmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve gblog",
	Run:   serve,
}

func serve(cmd *cobra.Command, args []string) {
	ctx := context.Background()

	mongoClient := getMongodbClient(ctx)
	postsCollection := mongoClient.Database("gblog").Collection("posts")
	defer func() {
		if err := mongoClient.Disconnect(ctx); err != nil {
			log.Fatal(err)
		}
	}()

	rdb := getRedisConnection()
	defer func() {
		if err := rdb.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	authDb := getAuthDatabase()
	defer func() {
		if err := authDb.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	authDb.AutoMigrate(&gblog.User{})

	postService := post.New(postsCollection)
	postService = applyPostMiddlewares(postService)

	authService := auth.New(authDb)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		auth.NewAuthInterceptor(),
		interceptor.NewCachingInterceptor(rdb),
	))
	postPb.RegisterPostStorageServer(grpcServer, postService)
	authPb.RegisterAuthServer(grpcServer, authService)
	reflection.Register(grpcServer)

	if err = grpcServer.Serve(lis); err != nil {
		log.Fatalln(err)
	}
}

func getMongodbClient(ctx context.Context) *mongo.Client {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		logrus.Panic(err)
	}

	if err = client.Connect(ctx); err != nil {
		logrus.Panic(err)
	}
	return client
}

func getRedisConnection() redis.Conn {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		log.Fatalf("can not connect to redis")
	}
	return c
}

func applyPostMiddlewares(service postPb.PostStorageServer) postPb.PostStorageServer {
	service = middleware.LoggingMiddlewareFactory(0)(service)
	service = middleware.LoggingMiddlewareFactory(1)(service)
	service = middleware.LoggingMiddlewareFactory(2)(service)
	return service
}

func getAuthDatabase() *gorm.DB {
	db, err := gorm.Open("sqlite3", "auth.sqlite3")
	if err != nil {
		log.Fatalln(err)
	}
	return db
}

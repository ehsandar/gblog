package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ehsandar/gblog/cmd/serve"
)

var RootCmd = &cobra.Command{
	Use:   "gblog",
	Short: "Gblog is very basic post project",
	Run:   nil,
}

func init() {
	RootCmd.AddCommand(serve.Cmd)
}

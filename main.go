package main

import (
	"gitlab.com/ehsandar/gblog/cmd"
	"os"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

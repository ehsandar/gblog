package gblog

import (
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type User struct {
	Username string `gorm:"primary_key;type:varchar(32)"`
	Password string `gorm:"type:varchar(255)"`
}

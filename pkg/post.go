package gblog

import "gitlab.com/ehsandar/gblog/pkg/gblog/post"

type PostServiceMiddleware func(post.PostStorageServer) post.PostStorageServer

.PHONY: cmd proto pkg internal

proto:
	rm -rf pkg/gblog
	mkdir pkg/gblog
	protoc -Iproto/ proto/*.proto --go_out=plugins=grpc:pkg
	protoc-go-inject-tag -input=pkg/gblog/post/post.pb.go
	protoc-go-inject-tag -input=pkg/gblog/auth/auth.pb.go
